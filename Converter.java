// package com.company;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

/*
    Java app converting from SI metric system to an Imperial
    Jedrzej Sawicki 2020
*/




/**
 * The type Converter.
 */
public class Converter {
    /**
     * The Convert number.
     */
    double convertNumber;
    /**
     * The Converted number.
     */
    double convertedNumber;
    /**
     * The Convert number type.
     */
    String convertNumberType;
    /**
     * The Converted number type.
     */
    String convertedNumberType;

    /**
     * Instantiates a new Converter.
     *
     * @param convertNumber the convert number
     */
    // constructor with one parameter, number to convert
    public Converter(double convertNumber) {
        this.convertNumber = convertNumber;
        System.out.println("Number to convert is : " + convertNumber);
    }

    /**
     * Kg to pound double.
     *
     * @param number the number
     * @return the double
     */
    // method to convert kg to pounds
    public double kgToPound(double number) {
        this.convertedNumber = number * 2.4419;
        this.convertNumberType = "kg";
        this.convertedNumberType = "funts";

        System.out.println(number +  " " + this.convertNumberType + " is a " + this.convertedNumber + " " + this.convertedNumberType);
        return this.convertedNumber;

    }

    /**
     * L to gallon double.
     *
     * @param number the number
     * @return the double
     */
    // method to convert liter to gallon
    public double lToGallon(double number) {
        this.convertedNumber = number * 0.264172;
        this.convertNumberType = "l";
        this.convertedNumberType = "Gallons";
        System.out.println(number +  " " + this.convertNumberType + " is a " + this.convertedNumber + " " + this.convertedNumberType);
        return this.convertedNumber;
    }

    /**
     * Cm to feet inch double.
     *
     * @param number the number
     * @return the double
     */
    // method to convert cm to feet
    public double cmToFeetInch(double number) {
        this.convertedNumber= number;
        // check if feets needed
        if (this.convertedNumber >= 30.48){
            this.convertNumberType = "Cm";
            this.convertedNumberType = "Feets";
            double convertedFoot = this.convertedNumber / 30.48;
            System.out.println(number +  " " + this.convertNumberType + " is a " + this.convertedNumber + " " + this.convertedNumberType);
        }
        //if
        else{
            double convertedInch = this.convertedNumber % 30.48;
            double convertedInches = convertedInch / 2.54;
            this.convertNumberType = "Cm";
            this.convertedNumberType = "Inches";
            System.out.println(number +  " " + this.convertNumberType + " is a " + this.convertedNumber + " " + this.convertedNumberType);
        }
        return this.convertedNumber;
    }


    /**
     * Km to miles double.
     *
     * @param number the number
     * @return the double
     */
    // method to convert km to miles
    public double kmToMiles(double number) {
        this.convertedNumber = number * 0.621371;
        this.convertNumberType = "Km";
        this.convertedNumberType = "Miles";
        System.out.println(number + " km is a " + this.convertedNumber + " miles");
        System.out.println(number +  " " + this.convertNumberType + " is a " + this.convertedNumber + " " + this.convertedNumberType);
        return this.convertedNumber;
    }

    /**
     * C to fahr double.
     *
     * @param number the number
     * @return the double
     */
    // method to convert celsius to fahrenheit
    public double cToFahr(double number) {
        this.convertedNumber = (number * 9 / 5) + 32;
        this.convertNumberType = "Celsius";
        this.convertedNumberType = "Fahrenheit";;
        System.out.println(number +  " " + this.convertNumberType + " is a " + this.convertedNumber + " " + this.convertedNumberType);
        return this.convertedNumber;
    }

    /**
     * M to yard double.
     *
     * @param number the number
     * @return the double
     */
    // method to convert m to yard
    public double mToYard(double number) {
        this.convertedNumber = number * 1.09361;
        this.convertNumberType = "Meters";
        this.convertedNumberType = "Yards";
        System.out.println(number +  " " + this.convertNumberType + " is a " + this.convertedNumber + " " + this.convertedNumberType);
        return this.convertedNumber;
    }

    /**
     * Save to file.
     *
     * @param converted the converted
     */
    //method to save the result of a conversion to a file
    public void saveToFile(double converted, String convertNumberType, String convertedNumberType) {
        this.convertedNumberType = convertedNumberType;
        this.convertNumberType = convertNumberType;

        System.out.println("Do you want to save the number to a file? (yes or no)");
        Scanner chooseString = new Scanner(System.in);
        String chosenString = chooseString.nextLine();
        if(chosenString.equals("yes")){
            System.out.println("1: Write a full path to a new file: \n2: Write file name (if it's in current directory)");
            String chosenNumber = chooseString.nextLine();
            switch(chosenNumber) {
                case "1":
                    System.out.println("Write the full path ending with filename (Prefered to add .txt at the end of the" +
                            " name");
                    try {
                        String str = chooseString.nextLine();
                        FileWriter fw = new FileWriter(new File(str));
                        fw.write( converted + " " + this.convertNumberType + " after conversion is:" +
                                this.convertedNumber + " " + this.convertedNumberType);
                        fw.close();
                        System.out.println("File saved!");
                    }
                    catch (IOException e) {
                        System.out.println("ERROR!");
                    }
                    break;
                case "2":
                    try{
                        System.out.println("Give the name for a new file (Prefered to add .txt at the end of the name");
                        String str = chooseString.nextLine();
                        String currentDir = System.getProperty("user.dir");
                        FileWriter fw = new FileWriter(new File(currentDir, str));
                        fw.write( converted + " " + this.convertNumberType + " after conversion is:" +
                                this.convertedNumber + " " + this.convertedNumberType);
                        fw.close();
                        System.out.println("File saved!");
                    }

                    catch (IOException e) {
                        System.out.println("ERROR");
                    }
                    break;


            }
        }
    }

    /**
     * Search for file.
     *
     * @param file the file
     */
    public static void searchForFileAndRunMenu(File file){
        try
        {
            //scanning for a file
            Scanner fl = new Scanner(file);
            //if its not empty, proceed
            while(fl.hasNextLine()){
                String inText = fl.nextLine();
                System.out.println(inText);
                //convert text given in txt file to a double
                double number = Double.parseDouble(inText);
                if(number < -273){
                    throw new IncorrectNumberTypeException("!!! Number given cant be below -273!");
                }
                //setting up object and activating the program
                Converter myConverter = new Converter(number);
                while(true){
                    myConverter.menu(number);
                }
            }
            fl.close();
        }
        //exception for a file missing and numberFormat
        catch (FileNotFoundException | NumberFormatException | IncorrectNumberTypeException e) {
            System.out.println("File not found! / Does the file contain number? " + e.getMessage());
            //throwing incorrectNumberTypeException
        }
    }

    /**
     * Get number and run menu.
     */
    public static void getNumberAndRunMenu(){
        System.out.println("Give me the number to convert");
        Scanner givenDouble = new Scanner(System.in);
        String givenText = givenDouble.nextLine();
        //  check if input is a number
        try {
            double number = Double.parseDouble(givenText);
            if(number < -273){
                throw new IncorrectNumberTypeException("!!! Number given cant be below -273!");
            }
            Converter myConverter = new Converter(number);
            while (true){
                myConverter.menu(number);

            }
        }
        catch (NumberFormatException | IncorrectNumberTypeException e) {
            System.out.println("Try again by giving me the right number! " + e.getMessage());
        }
    }


    /**
     * Menu.
     *
     * @param convertNumber the convert number
     */
    public void menu(double convertNumber) {
        // menu method, getting input and converting
        System.out.println("MENU");
        System.out.println(" 1: kg to pound \n 2: liter to gallon \n 3: cm to feet and inch \n 4: meter to yard \n 5: km to miles \n 6: Celsius to Fahrenheit \n 7: QUIT");
        boolean myBool = true;
        while (myBool) {
            Scanner sc = new Scanner(System.in);
            System.out.println("Start using menu, choose numbers from 1-7 (7 to exit).");
            int chosenNumber = sc.nextInt();
            switch (chosenNumber) {
                case 1:
                    kgToPound(convertNumber);
                    saveToFile(this.convertedNumber, this.convertNumberType, this.convertedNumberType);
                    break;
                case 2:
                    lToGallon(convertNumber);
                    saveToFile(this.convertedNumber, this.convertNumberType, this.convertedNumberType);
                    break;
                case 3:
                    cmToFeetInch(convertNumber);
                    saveToFile(this.convertedNumber, this.convertNumberType, this.convertedNumberType);;
                    break;
                case 4:
                    mToYard(convertNumber);
                    saveToFile(this.convertedNumber, this.convertNumberType, this.convertedNumberType);
                    break;
                case 5:
                    kmToMiles(convertNumber);
                    saveToFile(this.convertedNumber, this.convertNumberType, this.convertedNumberType);
                    break;
                case 6:
                    cToFahr(convertNumber);
                    saveToFile(this.convertedNumber, this.convertNumberType, this.convertedNumberType);
                    break;
                case 7:
                    System.out.println("see you soon!");
                    myBool = false;
                    // system exit, because of infinite while in main function
                    System.exit(1);
                    break;
                default:
                    System.out.println("wrong number!");
                    break;
            }
        }
    }

    /**
     * Welcome user.
     */
    public static void welcomeUser() {
        // method to welcome user
        System.out.println("Welcome to my converter! Do you want to read the number from a file? (yes or no)");
    }

    /**
     * The entry point of application.
     *
     * @param args the input arguments
     */
    public static void main(String[] args) {
        while(true){
            welcomeUser();

            Scanner chooseString = new Scanner(System.in);
            String chosenString = chooseString.nextLine();
            //getting answer for using the text file with if else
            if(chosenString.equals("yes")){
                System.out.println("1: Write a full path to a file: \n2: Write file name (if it's in current directory)");
                String chosenNumber = chooseString.nextLine();
                switch(chosenNumber){
                    case "1":
                        Scanner sc = new Scanner(System.in);
                        //setting the file path
                        String filePath = sc.nextLine();
                        //making file from File library with path
                        File file = new File(filePath);
                        searchForFileAndRunMenu(file);
                        break;
                    case "2":
                        Scanner sc2 = new Scanner(System.in);
                        //setting the file path
                        String fileName = sc2.nextLine();
                        String currentDir = System.getProperty("user.dir");
                        File filen = new File(currentDir, fileName);
                        searchForFileAndRunMenu(filen);
                }


            }
            // if answer is no
            else if(chosenString.equals("no")){
                getNumberAndRunMenu();

            }
            //  it is needed to give answer yes or no
            else{
                System.out.println("Answer yes or no!");
            }
        }
    }
}
