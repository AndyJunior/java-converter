// package com.company;

public class IncorrectNumberTypeException extends Exception {
    public IncorrectNumberTypeException(String errorMessage){
        super(errorMessage);
    }
}